No development in "master": please use one of the branches below instead.

Current branches and tags:

Drupal 8
  8.x-1.x Current development branch, tracking Drupal 8 core
  
Drupal 7
  7.x-1.x Current stable branch
    7.x-1.0 Current stable version
    7.x-1.0-beta1

Drupal 6
  6.x-1.x
    6.x-1.1-beta1
    6.x-1.0
    6.x-1.0-rc

Drupal 5 - No longer supported
  5.x-1.x
    5.x-1.2 Last stable version for D5 
    5.x-1.1
    5.x-1.1-rc
    5.x-1.0

Drupal 4.7 - No longer supported
  4.7.x-1.x
    4.7.x-1.0 Only stable version for D4.7

Drupal 4.6 - No Longer supported
  4.6.x-1.x
    4.6.x-1.0
